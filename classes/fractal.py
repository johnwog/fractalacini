#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 29 09:57:14 2017

@author: ioannisvogiatimageisoikonomidis
"""

import numpy as np
import sys, time, os, gc, pickle, signal
from math import ceil, log
import argparse
import multiprocessing as mproc
from skimage.io import imread
import csv
import matplotlib.pyplot as plt
import SimpleITK as sitk
import pseudoPolarFFT as pseudoPolarFFT
from scipy.integrate import simps

list_fractal = []
dict_fractal = {}
verbose = False

spiltCar = os.sep

largestAcinus = [1469,1037,1026]
largestVol = 1026.*1037.*1469.

def roundThousand(x):
    y = int(1000.0*x+0.5)
    return str(float(y)* .001)

def elapsedTime(start_time):
    dt = roundThousand(time.clock()-start_time)
    print "    ", dt, "seconds"
    sys.stdout.flush()

def NextPowerOfTwo(number):
    # Returns next power of two following 'number'
    return int(ceil(log(number,2)))

def boxCountingHistogram(image, numscales=20):
    """
    Adapted from https://francescoturci.net/2016/03/
    """    
    
    if isinstance(np.argwhere(image),np.ndarray):
        if image.ndimg>2:
            sys.exit("Function does NOT support 3D images.Exiting...")
        elif np.unique(image>0).shape[0]>1:
            sys.exit("Function supports ONLY binary images.Exiting...")
    else: 
        sys.exit("Input image must be a numpy array.Exiting...")
    Lx=image.shape[1]
    Ly=image.shape[0] 
    
    #get all non imageero pixels coords
    pixels = np.argwhere(image!=0)    
     
    # computing the fractal dimension
    #considering only scales in a logarithmic list
    scales=np.logspace(1, 8, num = numscales, endpoint=False, base=2)
    Ns=[]
    # looping over several scales
    for scale in scales:
        # computing the histogram
        H, edges=np.histogramdd(pixels, bins=(np.arange(0,Lx,scale),np.arange(0,Ly,scale)))
        Ns.append(np.sum(H))
     
    # linear fit, polynomial of degree 1
    coeffs=np.polyfit(np.log(scales), np.log(Ns), 1)
    
    return -coeffs[0]

def box2D(image, k):
    S = np.add.reduceat(
        np.add.reduceat(image, np.arange(0, image.shape[0], k), axis=0),
                           np.arange(0, image.shape[1], k), axis=1)

    # We count non-empty (0) and non-full boxes (k*k)
    return len(np.where((S > 0) & (S < k*k))[0]) 
    
def box3D(image, k):
    S = np.add.reduceat(
            np.add.reduceat(
                np.add.reduceat(image, np.arange(0, image.shape[0], k), axis=0),
                           np.arange(0, image.shape[1], k), axis=1),
                                np.arange(0, image.shape[2], k), axis=2)

    # We count non-empty (0) and non-full boxes (k*k)
    return len(np.where((S > 0) & (S < k*k*k))[0])  
    
def boxCounting(image, plot=False):
    global verbose 
    
    t = time.clock()
    if isinstance(image,np.ndarray):
        if not image.dtype == bool:
            image = image.astype(bool)
    #        width = np.max(image.shape)    # largest size of the box
    #        p = log(width)/log(2)   # nbre of generations
    #        
    #    #    % remap the array if the sizes are not all equal,
    #    #    % or if they are not power of two
    #    #    % (this slows down the computation!)
    #        if np.mod(width,2):
    #            widthNEW = 2**NextPowerOfTwo(width)
    #            if image.ndim ==2:
    #                mz = np.zeros(widthNEW, widthNEW)
    #                mz[0:image.shape[0], 0:image.shape[1]] = image
    #            elif image.ndim ==3:
    #                mz = np.zeros(widthNEW, widthNEW, widthNEW)
    #                mz[0:image.shape[0], 0:image.shape[1], 0:image.shape[2]] = image  
    #            image = mz           
                 
        # Minimal dimension of image
        NoPixels = (image/image.max()).sum()
        p = min(image.shape)
    
        # Greatest power of 2 less than or equal to p
        n = 2**np.floor(np.log(p)/np.log(2))
    
        # Extract the exponent
        n = int(np.log(n)/np.log(2))
    
        # Build successive box siimages (from 2**n down to 2**1)
        siimages = 2**np.arange(n, 1, -1)
    
        # Actual box counting with decreasing siimage
        counts = []
        for siimage in siimages:
            if image.ndim == 2:
                counts.append(box2D(image, siimage))
            elif image.ndim==3:
                counts.append(box3D(image, siimage))
            else:
                sys.exit("Only 2D or 3D binary arrays are supported.Exiting...")
        
        # Fit the successive log(siimages) with log (counts)
        try:
            coeffs = np.polyfit(np.log(siimages), np.log(counts), 1)
        except:
            coeffs = np.zeros(2)
            
        if plot:
            fig, ax = plt.subplots()
            ax.loglog(siimages,counts, 'o', mfc='none') 
            ax.loglog(siimages,np.exp(coeffs[1])*(siimages**coeffs[0]))
            ax.set_xlabel('Box Size')                
            ax.set_ylabel('Number of Boxes')
            plt.grid(which='minor', ls='-', color='0.75')
            plt.grid(which='major', ls='-', color='0.25')
            plt.show()

        elapsedTime(t)

        #pl.plot(np.log(siimages),np.log(counts), 'o', mfc='none')
        #pl.plot(np.log(siimages), np.polyval(coeffs,np.log(counts)))
        if verbose:
            print 'Average fractal dimension  is:',-coeffs[0]  
            sys.stdout.flush()
        
        extDict = {'coeffs':coeffs.tolist(), 'scales':siimages.tolist(), 'counts':counts, 
                   'NoPixels': [str(NoPixels)] , 'method':'box'}  
 
        return extDict 
       
    else: 
        sys.exit("Input image must be a 2D or 3D binary numpy array.Exiting...")  

def correlationDimension(volume, plot = False):
    """ 
    correlationDimension Compute fractal dimensions 

    """
    global verbose 
    if isinstance(volume,np.ndarray):
        if not volume.dtype == bool:
            if verbose:
                print 'Acinus not a binary array converting.'
                sys.stdout.flush()
            volume = volume.astype(bool)
        if volume.ndim!=3:
            sys.exit("Function ONLY binary 3D volumes.Exiting...")

    N1, N2, N3 = volume.shape

    if not N1 == N2:
        if verbose:
            print 'Acinar X-Y dimensions not the same.Padding for polar fourier consistency...'
            sys.stdout.flush()
#        [N1,N2]=size(X);
#        N=ceil(max(N1,N2)/2)*2;
#        Xnew=zeros(N,N);
#        Xnew(N/2-floor(N1/2)+1:N/2-floor(N1/2)+N1,N/2-floor(N2/2)+1:N/2-floor(N2/2)+N2)=X;
#        X=Xnew;
        N = int(np.ceil(0.5*np.max([N1,N2]))*2)
        Xnew = np.zeros((N,N,N3), dtype = volume.dtype)
        Xnew[int(N/2-np.floor(N1/2)):int(N/2-np.floor(N1/2)+N1),\
             int(N/2-np.floor(N2/2)):int(N/2-np.floor(N2/2)+N2),:] = volume
        volume = Xnew.copy()
        Xnew = None
        gc.collect()

    t = time.clock()
    NoPixels = (volume/volume.max()).sum()

    if verbose:
        print 'Computing Polar FFT...'
        sys.stdout.flush()

    volume = pseudoPolarFFT.PolarFFT3(volume)
    if type(volume) is None:
        sys.exit('Fractal estimation failed!')

    if verbose:
        print 'Polar FFT done!'
        sys.stdout.flush()
        
    volume = volume * volume.conj()
    volume = volume.real

#2N since PseudopolarFFT3 double the size
    n1,n2,n3 = volume.shape
#    x,y  = np.meshgrid(range(-n1,n1),range(-n2,n2))   
#    r = np.sqrt(x**2 + y**2)
#    
#    GridX, GridY = pseudoPolarFFT.CreateOversampledGrid('X', N)
#    GridZ = np.sqrt(GridX**2 + GridY**2)
    
    siimages = range(n1) #np.unique(GridZ.ravel())  
    
    dtheta = np.linspace(0,np.pi,num = volume.shape[2])
    dphi = np.linspace(0,2*np.pi,num = volume.shape[0])
    
    FK = np.zeros_like(siimages) 
    
    for siimage,theta in zip(siimages,dtheta):
        integrate  = volume[siimage,:,:]
#        if siimage == 0:
#            FK[siimage] = integrate.sum() * 0.5 * np.pi
#        else:
#            integrate = integrate.reshape(-1, integrate.shape[-1]) * np.sin(theta)
#            FK[siimage] = simps(simps(integrate, dphi), dtheta)/(4 * np.pi)
        integrate = integrate.reshape(-1, integrate.shape[-1]) * np.sin(theta)
 
        sys.stdout.flush()
        FK[siimage] = simps(simps(integrate, dphi, axis=0), dtheta)/(4 * np.pi)
            
    # Fit the successive log(siimages) with log (counts)
    try:
        coeffs = np.polyfit(np.log(siimage), np.log(FK), 1)
    except:
        coeffs = np.zeros(2)
    
    print coeffs
    sys.stdout.flush()
    
    if plot:
        fig, ax = plt.subplots()
        ax.loglog(siimages,FK, 'o', mfc='none') 
        ax.loglog(siimages,np.exp(coeffs[1])*(siimages**coeffs[0]))
        ax.set_xlabel('Box Size')                
        ax.set_ylabel('Number of Boxes')
        plt.grid(which='minor', ls='-', color='0.75')
        plt.grid(which='major', ls='-', color='0.25')
        plt.show()

    #pl.plot(np.log(siimages),np.log(counts), 'o', mfc='none')
    #pl.plot(np.log(siimages), np.polyval(coeffs,np.log(counts)))
    if verbose:
        print 'Average fractal dimension  is:',-coeffs[0]  
        sys.stdout.flush()

    extDict = {'coeffs':coeffs.tolist(), 'scales':siimages, 'counts':FK, 
               'NoPixels': [str(NoPixels)] , 'method':'cor'}         

    elapsedTime(t)

    return extDict

def collect_result(result):
    # This is called whenever foo_pool(i) returns a result.
    # result_list is modified only by the main process, not the pool workers.
    dict_fractal.update(result)
 
def getAllFiles(directory, fType):
    import glob
    
    if not fType[0] == '.':
        string = os.path.join(directory, '*.' + fType)
    else:
        string = os.path.join(directory, '*' + fType)
        
    filelist = glob.glob(string)

    if not len(filelist): 
        raise ValueError("Input directory empty!")
#        print 'ERROR: Input directory empty!'
#        sys.stdout.flush()
#        sys.exit()

    return filelist
    
def computeFractal(filePath, method):
    global verbose
    try:
        if verbose:
            print "Estimating fractal dimension for",filePath,"\nPlease wait.."
            sys.stdout.flush()

        obj = imread(filePath)
        NoPixels = (obj/255).sum()
        if verbose:
            print "Resizing acinus to the scale of the largest one..."
            sys.stdout.flush()
            
#        scale = np.array(largestAcinus)/np.array(obj.shape,dtype=float)
#        scale = np.round(scale)
#        scale = scale.astype(np.uint32)
#        acinusLarge = sitk.GetImageFromArray(obj)
#        #permute scale for SITK conversion
#        scale = scale[[2,1,0]].tolist()

        #scaling to the largest volume!
        scale = largestVol/float(obj.size)
        scale = np.round(scale**(1./3))
        scale = [int(scale)]*3
        scale = np.asarray(scale, dtype = np.uint32)   
        scale = scale.tolist()
        print "Resize scale is: ",scale
        sys.stdout.flush()
        acinusLarge = sitk.GetImageFromArray(obj)
        acinusLarge = sitk.Expand(acinusLarge,scale,sitk.sitkNearestNeighbor)
        obj = sitk.GetArrayFromImage(acinusLarge)
        acinusLarge = None
        gc.collect()
        print "Resizing done..."
        sys.stdout.flush()
#           
        if method == 'box':
            if verbose:
                print "Started box counting..."
                sys.stdout.flush()
            extDict = boxCounting(obj)
            extDict['NoPixels']= [str(NoPixels)]
            return {filePath.split(spiltCar)[-1]: extDict}

        if method == 'cor':
            if verbose:
                print "Started correlation dimension..."
                sys.stdout.flush()
            extDict = correlationDimension(obj)
            extDict['NoPixels']= [str(NoPixels)]            
            return {filePath.split(spiltCar)[-1]: extDict}

        elif method == 'both':
            if verbose:
                print "Started box counting..."
                sys.stdout.flush()
            extDictBox = boxCounting(obj)
            extDictBox['NoPixels']= [str(NoPixels)]

            if verbose:
                print "Started correlation dimension..."
                sys.stdout.flush()
            extDictCor = correlationDimension(obj)

            return {filePath.split(spiltCar)[-1]: extDictBox.update(extDictCor)} 
    except: 
        if verbose:
            print 'Fractal estimation for ',filePath.split(os.sep)[-1],' failed'
            sys.stdout.flush()
        NoPixels = (obj/obj.max()).sum()
        extDict = {'coeffs':[0,0], 'scales':[0,0], 'counts':[0,0], 'NoPixels': [str(NoPixels)]}

        return {filePath.split(spiltCar)[-1]: extDict} 

def getArgs():
    parser = argparse.ArgumentParser(description='Computes the fractal dimension of the acini. Input is binary exctracted 3D acini in .tif file',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    
    parser.add_argument('-inp', '--input', dest='input', default = None, type = str,
                        help = 'Specify the image to be segmented') 

    parser.add_argument('-t', '--type', dest='type', default = 'binary.tif', type = str,
                        help = 'Specify file type')  

    parser.add_argument('-m', '--method', dest='method', default = 'box', type = str, choices = ['box','cor','both'] ,
                        help = 'Specify whether you want to use box counting or correllation dimention method' )                            

    parser.add_argument('-p', '--parallel', dest='parallel', action='store_true', 
                        help = 'Compute in parallel' ) 
                        
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', 
                        help = 'Compute in parallel' )                            
                        
    args = parser.parse_args()
    
    return args        

def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)

def main():
    global dict_fractal
    global verbose    
    
    args = getArgs()  
    verbose = args.verbose
    
    folder = args.input

    if os.path.isdir(folder):

        fileList = getAllFiles(folder, args.type)
        methods = [args.method]*len(fileList)
        if args.parallel:
            if verbose:
                print "Submitting slices to cores for extracting.Please wait..."
                sys.stdout.flush()
            pool = mproc.Pool(processes=3, initializer=init_worker)  #int(mproc.cpu_count()/10)      
             
            for i in range( len(fileList) ):
                pool.apply_async( computeFractal , (fileList[i], methods[i]), callback = collect_result) 
                sys.stdout.flush()
              
            try:
    #            print "Waiting 10 seconds for keyboard interrupt"
                time.sleep(10)
        
            except KeyboardInterrupt:
                print "Caught KeyboardInterrupt, terminating workers"
                sys.stdout.flush()
                pool.terminate()
                pool.join()
        
            else:
                print "Quitting normally"
                sys.stdout.flush()
                pool.close()
                pool.join()
        else:
            if verbose:
                print "WARNING!!!Non parallel computation started..."
                sys.stdout.flush()
            for i in range( len(fileList) ):
                result = computeFractal(fileList[i], methods[i]) 
                collect_result(result)
        
    elif os.path.isfile(folder):
        result = computeFractal(folder,args.method)
        collect_result(result)
    else:
        sys.exit('Wrong input')

    if verbose:    
        print 'Dimensions computed.Writing results...'
        sys.stdout.flush()

    if os.path.isdir(folder):
        csvName = folder + '.dimensions' + args.method + '.csv'
    else:
        csvName = os.path.dirname(folder) + '.dimensions' + args.method + '.csv'

    if os.path.isdir(folder):
        pickleName = folder + '.dimensions'+ args.method + '.pickle'
    else:
        pickleName = os.path.dirname(folder) + '.dimensions' + args.method + '.pickle'

    #check if file already exists cause you can't append to pickle file
    #if it does load the stored dictionary update the current one delete the pickle 
    #and store the updated version 
    if not os.path.isfile(pickleName):
        with open(pickleName, 'wb') as handle:
            pickle.dump(dict_fractal, handle, protocol=pickle.HIGHEST_PROTOCOL)
    else:
        #accessing global dictionary
        dict_fractalCur = dict_fractal.copy()
        #loading old results
        with open(pickleName, 'rb') as handle:
            dict_fractal = pickle.load(handle)
        #storing old and current results to tmp dictionary
        dict_fractalCur.update(dict_fractal)  
        #delete old pickle
        os.remove(pickleName)
        #storing to global dictionary again
        dict_fractal = dict_fractalCur.copy()
        #writing updated pickle
        with open(pickleName, 'wb') as handle:
            pickle.dump(dict_fractal, handle, protocol=pickle.HIGHEST_PROTOCOL)
 

    #storing results in CSV
    with open(csvName, 'a') as myfile:
        w = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        for key, val in dict_fractal.items():
            a = []
            a = a + [key]
            for key1, val1 in val.items():
                if not type(val1) == list:
                    val1 = [val1]
                a = a + [key1] + val1
            w.writerow(a)
    if verbose:
        print 'Fractals computed and written! THANK YOU FOR FLYING TOMCAT!!!'
        sys.stdout.flush()
            
if __name__ == '__main__':

    main()

#https://francescoturci.net/2016/03/31/box-counting-in-numpy/
#http://connor-johnson.com/2014/03/04/fractal-dimension-and-box-counting/
#https://gist.github.com/rougier/e5eafc276a4e54f516ed5559df4242c0