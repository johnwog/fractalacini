#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 18 16:21:39 2017

@author: ioannisvogiatzisoikonomidis
"""

"""
AXIS ONE IN MATLAB MEANS PER COLUMN
AXIS ONE IN PYTHON MEANS PER ROW
"""

import numpy as np
import scipy.fftpack
import sys, gc
from scipy.interpolate import InterpolatedUnivariateSpline 
import matplotlib.pyplot as plt

#TODO debug PseudopolarFFTn

def PolarFFT3(X, method = 'polar'):
    methods = ['polar', 'pseudo']

    if method not in methods:
        raise ValueError("Invalid polar FFT. Expected one of: %s" % methods)

    if method == 'pseudo':
        X = scipy.fftpack.fft(X, axis = 2, overwrite_x = True)
        X = scipy.fftpack.fftshift(X, axes = 2)
        Y = PseudopolarFFTn(X)
    else:
        X = scipy.fftpack.fft(X, axis = 2, overwrite_x = True)
        X = scipy.fftpack.fftshift(X, axes = 2)
        Y = PFFT(X)

    return Y.astype(np.complex64)

def PseudopolarFFTn(X, verbose = True):
    """ 
    ported to python from http://www.physics.ucla.edu/research/imaging/EST/
    """
#    N = X.shape[0]
#    if not X.shape[0] == X.shape[1]:
#        sys.exit('Zeroth and first dimension must be equal')
    N1, N2, N3 = X.shape
    N = N1
    if not N1 == N2:        
        N = int(np.ceil(0.5*np.max([N1,N2]))*2)
        Xnew = np.zeros((N,N,N3), dtype = X.dtype)
        Xnew[int(N/2-np.floor(N1/2)):int(N/2-np.floor(N1/2)+N1),\
             int(N/2-np.floor(N2/2)):int(N/2-np.floor(N2/2)+N2),:] = X
        X = Xnew.copy()
        Xnew = None
        gc.collect()

    if X.ndim == 3:
        N3 = X.shape[2]
        if N3 != 1:
            F1 = 1
            F2 = 2
    else:
        N3 = 1
        F1 = 3
        F2 = 4

    Y = np.zeros((2*N,2*N,N3), dtype = np.complex64)

    #Stage 1: Constructing quadrant 1 and 3
    if verbose:
        print "Stage 1: Constructing quadrant 1 and 3..."
        sys.stdout.flush()
    f_tilde = np.zeros((2*N,N,N3), dtype = np.complex64)
    f_tilde[N/2:3*N/2,:,:] = X 
    if verbose:
        print "Computing Centered FFT..."
        sys.stdout.flush()
    f_tilde = My_FFT_Centered(f_tilde,2*N,F1)
    if verbose:
        print "Computing Centered Fractional FT..."
        sys.stdout.flush()
    for ll in range(-N,N):
        Y[:N,ll+N,:] = My_FRFT_Centered(f_tilde[ll+N,:,:],-float(ll/N),N,N3)
    
    #Stage 2: Constructing quadrant 2 and 4  
    if verbose:
        print "Stage 2: Constructing quadrant 2 and 4..."
        sys.stdout.flush()
    f_tilde = np.zeros((N,2*N,N3), dtype = np.complex64)
    f_tilde[:,N/2:3*N/2,:] = X
    if verbose:
        print "Computing Centered IFFT..."
        sys.stdout.flush()
    f_tilde = My_IFFT_Centered(f_tilde,2*N,F2)
    if verbose:
        print "Computing Centered Fractional FT..."
        sys.stdout.flush()
    for ll in range(-N,N):
        Y[N:2*N,ll+N,:] = My_FRFT_Centered(f_tilde[:,ll+N,:],-float(ll/N),N,N3)

    Y = Y/(N*np.sqrt(2))
    
    return Y

def PFFT(X, OS1=2, OS2=2, verbose = True):
    """
    This function performs a Fourier Transfrom over the polar grid. It uses
    Hermite interpolation along the angles, and spline one for the rays
    
    ported from Polar Lab MATLAB package    
    
    Parameters
    ----------
        X: The Input as a N*N*M array holding the signal. \n
        OS1:  Oversampling factor for the S-Polar transform (equiangle but still with squares) \n
        OS2: Oversampling factor for resampling of the rays. \n
        
    Returns
    ----------
        Out: The Output as a 2N by 2N new values that correpsonds to a new 
                                X-Polar grid (Polar grid with 2N samples along each ray).
    """ 
#TODO implement non for loop for PFFT
#    N = X.shape[0]
#    if not X.shape[0] == X.shape[1]:
#        sys.exit('Zeroth and first dimension must be equal')
    N1, N2, N3 = X.shape
    N = int(np.ceil(0.5*np.max([N1,N2]))*2)
    Xnew = np.zeros((N,N,N3), dtype = X.dtype)
    Xnew[int(N/2-np.floor(N1/2)):int(N/2-np.floor(N1/2)+N1),\
         int(N/2-np.floor(N2/2)):int(N/2-np.floor(N2/2)+N2),:] = X
    X = Xnew.copy()
    Xnew = None
    gc.collect()

    if X.ndim == 3:
        N3 = X.shape[2]
        if N3 != 1:
            F1 = 1
            F2 = 2
    else:
        N3 = 1
        F1 = 3
        F2 = 4
    #-------------------------------------------------------------------------------------------- 
    #                        Stage 1 - Basically Vertical Rays
    #--------------------------------------------------------------------------------------------
    if verbose:
        bold = "\033[1m"
        reset = "\033[0;0m"
        print bold + 'Stage 1 - Basically Vertical Rays' + reset
        sys.stdout.flush()
    # A. FFT on the columns with zero padding       
    if verbose:
        print 'FFT on the columns with zero padding. Please wait...' 
        sys.stdout.flush()
#    padWidth = (((2*OS2-1)*X.shape[0],0),(0,0),(0,0))    
#    f_tilde = np.pad(X, padWidth,'constant',constant_values=0)

    f_tilde = scipy.fftpack.fft(X, 2*OS2*X.shape[0], axis = 0)
    f_tilde = scipy.fftpack.fftshift(f_tilde, axes = 0)
    f_tilde = f_tilde.astype(np.complex64)
    
#    f_tilde = np.zeros((2*OS2*X.shape[0],N,N3), dtype = np.complex64)
#    f_tilde[N/2:3*N/2,:,:] = X 
#    if verbose:
#        print "Computing Centered FFT..."
#        sys.stdout.flush()
#    f_tilde = My_FFT_Centered(f_tilde,2*OS2*X.shape[0],F1)

    # B. FFT on the rows while putting the grid points on the S-Polar   
    if verbose:
        print 'FFT on the rows while putting the grid points on the S-Polar. Please wait...'   
        sys.stdout.flush()                 
    Xcor1 = 2*np.pi/(N*OS2) * np.arange(-OS1*N/2., OS1*N/2., 1.)/OS1/N
    Xcor2 = np.pi/(N*OS2) * np.tan(np.pi * np.arange(-N/2., N/2., 1.)/N/2.) 
    F = np.zeros((2*OS2*N,N,N3), dtype=np.complex64)
    
    for ll in range(-N*OS2, N*OS2):   
        padded_row = np.pad(f_tilde[ll+OS2*N,:,:],((0, OS1*N-N), (0, 0)),'constant',constant_values=0)
        Temp1 = My_FRFT_Centered(padded_row,float(ll)/N**2/OS1/OS2,OS1*N,N3) 
        
#        Temp1 = My_FRFT(padded_row,-ll/N**2/OS1/OS2,OS1*N,N3)  
#        Vec=[f_tilde(ll+OS2*N+1,:),zeros(1,OS1*N-N)]
#        alpha=ll/N^2/OS1/OS2      
#        Temp1=My_FRFT_Centered(Vec,alpha).'
#        Vec=Vec.*(-ii*[0:1:OS1*N-1])
#        Temp2=My_FRFT_Centered(Vec,alpha).' % first derivative
#        Vec=Vec.*(-ii*[0:1:OS1*N-1])
#        Temp3=My_FRFT_Centered(Vec,alpha).' % second derivativeif verbose:
        for kk in range(N3):
            if verbose:
                if not(abs(ll) % 50) and not(abs(kk) % 50):
                    sys.stdout.write("\r  Interpolating row %d for S-polar ray %d " % (ll, kk))
                    sys.stdout.flush()             
            if ll != 0:
            #TODO change to Hermitian interpolation
                if not ll*Xcor1[0]>ll*Xcor1[-1]: # scipy expects x values in ascending order
                    splineReal = InterpolatedUnivariateSpline(ll*Xcor1, Temp1[:,kk].real, k=3)
                    splineImag = InterpolatedUnivariateSpline(ll*Xcor1, Temp1[:,kk].imag, k=3)
                    F[ll+N*OS2,:,kk] = splineReal(ll*Xcor2) + 1j* splineImag(ll*Xcor2)
                else: 
                    splineReal = InterpolatedUnivariateSpline(ll*Xcor1[::-1], Temp1[:,kk].real[::-1], k=3)
                    splineImag = InterpolatedUnivariateSpline(ll*Xcor1[::-1], Temp1[:,kk].imag[::-1], k=3)
                    F[ll+N*OS2,:,kk] = (splineReal(ll*Xcor2[::-1]) + 1j* splineImag(ll*Xcor2[::-1]))[::-1]                   
            else:
                F[ll+N*OS2,:,kk] = Temp1[:,kk][::OS1]

    if verbose:
        sys.stdout.write("\r  \r\n") # clean up 
    #releasig memory
    padded_row = None
    Temp1 = None
    Xcor1 = None
    Xcor2 = None
    splineReal = None
    splineImag = None
    f_tilde = None
    gc.collect()
    
    # C. resampling the rays to the X-Polar locations
    Fout = np.zeros((2*N,N,N3), dtype=np.complex64)
    if verbose:
        print 'Resampling the rays to the X-Polar locations. Please wait...' 
        sys.stdout.flush()
    Xcor = np.arange(-np.pi, np.pi-np.pi/(2.*N*OS2), 2*np.pi/(2.*N*OS2)) # these are the projected locations
    for ll in range(N):
        for kk in range(N3):
            Ray = F[:, ll, kk]
            Factor = np.cos((ll-N/2.-1)*np.pi/(2*N))
            if not(abs(ll) % 50) and not(abs(kk) % 50):
                sys.stdout.write("\r  Resampling at %d for S-polar ray %d " % (ll, kk))
                sys.stdout.flush() 
            if not (Xcor/Factor)[0]>(Xcor/Factor)[-1]:
                splineReal = InterpolatedUnivariateSpline(Xcor/Factor, Ray.real, k=3)
                splineImag = InterpolatedUnivariateSpline(Xcor/Factor, Ray.imag, k=3)
                Fout[:,ll,kk] = splineReal(Xcor[::OS2]) + 1j* splineImag(Xcor[::OS2])
            else:
                splineReal = InterpolatedUnivariateSpline((Xcor/Factor)[::-1], Ray.real[::-1], k=3)
                splineImag = InterpolatedUnivariateSpline((Xcor/Factor)[::-1], Ray.imag[::-1], k=3)
                Fout[:,ll,kk] = (splineReal(Xcor[::-1][::OS2]) + 1j* splineImag(Xcor[::-1][::OS2]))[::-1]
    
    #releasig memory
    F = None
    Factor = None
    Xcor = None
    splineReal = None
    splineImag = None
    gc.collect()
    #-------------------------------------------------------------------------------------------- 
    #                        Stage 2 - Basically Horizontal Rays
    #--------------------------------------------------------------------------------------------
    if verbose:
        bold = "\033[1m"
        reset = "\033[0;0m"
        print bold + 'Stage 2 - Basically Horizontal Rays' + reset
        sys.stdout.flush()
    # A. FFT on the rows with zero padding
    if verbose:
        print 'FFT on the rows with zero padding. Please wait...' 
        sys.stdout.flush()
#    pad = np.zeros((X.shape[0], (2*OS2-1)*X.shape[1], X.shape[2]))
#    f_tilde = np.hstack((X,pad))
#    #releasing memory    
#    pad = None
#    gc.collect()
#
#    f_tilde = scipy.fftpack.fft(f_tilde, axis = 1, overwrite_x=True)
#    f_tilde = scipy.fftpack.fftshift(f_tilde, axes = 1)
#    f_tilde=f_tilde.transpose(1, 0, 2)
        
    f_tilde = scipy.fftpack.fft(X, 2*OS2*X.shape[1], axis = 1)
    f_tilde = scipy.fftpack.fftshift(f_tilde, axes = 1)
    f_tilde = f_tilde.astype(np.complex64)
    f_tilde=f_tilde.transpose(1, 0, 2)
    
#    f_tilde = np.zeros((N,2*OS2*X.shape[1],N3), dtype = np.complex64)
#    f_tilde[:,N/2:3*N/2,:] = X
#    if verbose:
#        print "Computing Centered IFFT..."
#        sys.stdout.flush()
#    f_tilde = My_IFFT_Centered(f_tilde,2*OS2*X.shape[1],F2)
#    f_tilde=f_tilde.transpose(1, 0, 2)

    # B. FFT on the rows while putting the grid points on the S-Polar   
    if verbose:
        print 'FFT on the rows while putting the grid points on the S-Polar. Please wait...'   
        sys.stdout.flush()                 
    Ycor1 = 2*np.pi/(N*OS2) * np.arange(-OS1*N/2.+1, OS1*N/2.+1, 1.)/OS1/N
    Ycor2 = np.pi/(N*OS2) * np.tan(np.pi * np.arange(-N/2.+1, N/2.+1, 1.)/N/2.) 
    G = np.zeros((2*OS2*N,N,N3), dtype=np.complex64)
    for ll in range(-N*OS2, N*OS2): 
        Factor = np.exp(1j*2*np.pi*np.arange(0,OS1*N) * (OS1*N/2.-1)*float(ll)/N**2/OS1/OS2)    
        Factor = np.tile(Factor, (N3,1))
        Factor = Factor.transpose()
        padded_row = np.pad(f_tilde[ll+OS2*N,:,:],((0, OS1*N-N), (0, 0)),'constant',constant_values=0)
        Temp1 = My_FRFT(padded_row*Factor,float(ll)/N**2/OS1/OS2,OS1*N,N3) 
        
#        Temp1 = My_FRFT(padded_row,-ll/N**2/OS1/OS2,OS1*N,N3)  
#        Vec=[f_tilde(ll+OS2*N+1,:),zeros(1,OS1*N-N)]
#        alpha=ll/N^2/OS1/OS2      
#        Temp1=My_FRFT_Centered(Vec,alpha).'
#        Vec=Vec.*(-ii*[0:1:OS1*N-1])
#        Temp2=My_FRFT_Centered(Vec,alpha).' % first derivative
#        Vec=Vec.*(-ii*[0:1:OS1*N-1])
#        Temp3=My_FRFT_Centered(Vec,alpha).' % second derivativeif verbose:
        for kk in range(N3):
            if verbose:
                if not(abs(ll) % 50) and not(abs(kk) % 50):
                    sys.stdout.write("\r  Interpolating row %d for S-polar ray %d " % (ll, kk))
                    sys.stdout.flush()             
            if ll != 0:
            #TODO change to Hermitian interpolation
                if not ll*Ycor1[0]>ll*Ycor1[-1]: # scipy expects x values in ascending order
                    splineReal = InterpolatedUnivariateSpline(ll*Ycor1, Temp1[:,kk].real, k=3)
                    splineImag = InterpolatedUnivariateSpline(ll*Ycor1, Temp1[:,kk].imag, k=3)
                    G[ll+N*OS2,:,kk] = splineReal(ll*Ycor2) + 1j* splineImag(ll*Ycor2)
                else: 
                    splineReal = InterpolatedUnivariateSpline(ll*Ycor1[::-1], Temp1[:,kk].real[::-1], k=3)
                    splineImag = InterpolatedUnivariateSpline(ll*Ycor1[::-1], Temp1[:,kk].imag[::-1], k=3)
                    G[ll+N*OS2,:,kk] = (splineReal(ll*Ycor2[::-1]) + 1j* splineImag(ll*Ycor2[::-1]))[::-1]                   
            else:
                G[ll+N*OS2,:,kk] = Temp1[:,kk][::OS1]

    #releasig memory
    padded_row = None
    Temp1 = None
    Xcor1 = None
    Xcor2 = None
    Factor = None
    splineReal = None
    splineImag = None
    f_tilde = None
    gc.collect()
    
    # C. resampling the rays to the X-Polar locations
    Gout = np.zeros((2*N,N,N3),dtype=np.complex64)
    if verbose:
        print 'Resampling the rays to the X-Polar locations. Please wait...'     
        sys.stdout.flush()
    Xcor = np.arange(-np.pi, np.pi-np.pi/(2*N*OS2), 2*np.pi/(2*N*OS2)) # these are the projected locations
    for ll in range(N):
        for kk in range(N3):
            Ray = G[:, ll, kk]
            Factor = np.cos((ll-N/2.-1)*np.pi/(2*N))
            if not(abs(ll) % 50) and not(abs(kk) % 50):
                sys.stdout.write("\r  Resampling at %d for S-polar ray %d " % (ll, kk))
                sys.stdout.flush() 
            if not (Xcor/Factor)[0]>(Xcor/Factor)[-1]:
                splineReal = InterpolatedUnivariateSpline(Xcor/Factor, Ray.real, k=3)
                splineImag = InterpolatedUnivariateSpline(Xcor/Factor, Ray.imag, k=3)
                Gout[:,ll,kk] = splineReal(Xcor[::OS2]) + 1j* splineImag(Xcor[::OS2])
            else:
                splineReal = InterpolatedUnivariateSpline((Xcor/Factor)[::-1], Ray.real[::-1], k=3)
                splineImag = InterpolatedUnivariateSpline((Xcor/Factor)[::-1], Ray.imag[::-1], k=3)
                Gout[:,ll,kk] = (splineReal(Xcor[::-1][::OS2]) + 1j* splineImag(Xcor[::-1][::OS2]))[::-1]
 
    #releasig memory
    G = None
    Factor = None
    Xcor = None
    splineReal = None
    splineImag = None
    gc.collect()

    #-------------------------------------------------------------------------------------------- 
    #                        Stage 3 - Merging the two Transform sections
    #--------------------------------------------------------------------------------------------
    if verbose:
        bold = "\033[1m"
        reset = "\033[0;0m"
        print bold + 'Stage 3 - Merging the two Transform sections' + reset
        sys.stdout.flush()

    Out = np.hstack((np.fliplr(Fout),Gout)) #we have to flip in order to get a smoothly rotating rays

    return Out

def My_FFT_Centered(X,N,F):
    NC = int(N)/2

    if F == 1:    
        X = np.roll(X, -(NC-1), axis = 0)
        X = scipy.fftpack.fft(X, axis = 0, overwrite_x = True)
        Y = scipy.fftpack.fftshift(X, axes = 0)
    elif F == 2:
        X = np.roll(X, -(NC-1), axis = 1)
        X = scipy.fftpack.fft(X, axis = 1, overwrite_x = True)
        Y = scipy.fftpack.fftshift(X, axes = 1)
    elif F == 3:
        X = np.roll(X, -(NC-1), axis = 0)
        X = scipy.fftpack.fft(X, axis = 0, overwrite_x = True)
        Y = scipy.fftpack.fftshift(X, axes = 0)    
    elif F == 4:
        X = np.roll(X, -(NC-1), axis=1)
        X = scipy.fftpack.fft(X, axis = 1, overwrite_x = True)
        Y = scipy.fftpack.fftshift(X, axes = 1)  

    return Y.astype(np.complex64)

def My_IFFT_Centered(X,N,F):
    NC = int(N)/2

    if F == 1:    
        X = np.roll(X, -(NC-1), axis = 0)
        X = scipy.fftpack.ifft(X, axis = 0, overwrite_x = True)
        Y = scipy.fftpack.fftshift(X , axes = 0)
    elif F == 2:
        X = np.roll(X, -(NC-1), axis = 1)
        X = scipy.fftpack.ifft(X, axis = 1,overwrite_x=True)
        Y = scipy.fftpack.fftshift(X, axes = 1)
    elif F == 3:
        X = np.roll(X,-(NC-1), axis = 0)
        X = scipy.fftpack.ifft(X, axis = 0, overwrite_x = True)
        Y = scipy.fftpack.fftshift(X, axes = 0)    
    elif F == 4:
        X = np.roll(X,-(NC-1), axis = 1)
        X = scipy.fftpack.ifft(X, axis = 1, overwrite_x = True)
        Y = scipy.fftpack.fftshift(X, axes = 1)  

    return Y.astype(np.complex64)


def My_FRFT_Centered(X,alpha,N,N3):
#    alpha = float(alpha)
    if X.ndim >= 2:        
        preFactor = np.dot(np.vstack(np.exp(1j*np.pi*np.arange(0,N)* N* alpha)),np.ones((1,N3)))
        X = X*preFactor
        n = np.hstack((np.arange(0,N),np.arange(-N,0)))
        n = np.vstack(n) #transposing 
        n = np.dot(n, np.ones((1,N3)))
#        Factor = np.exp(-(1j*np.pi*alpha*n**2)/N) #EST: My_FRFT_Centered
        Factor = np.exp(-1j*np.pi*alpha*n**2)
        
        x_tilde = np.vstack((X,np.zeros((N,N3))))
        x_tilde = x_tilde*Factor    
    
        x_tilde = scipy.fftpack.fft(x_tilde, axis = 0, overwrite_x=True)
        YY = scipy.fftpack.fft(np.conjugate(Factor), axis = 0)
        Y = scipy.fftpack.ifft(x_tilde*YY, axis = 0)
        Y = Y * Factor
        
        Y = Y[0:N,:]
    
#        postFactor = np.dot(np.vstack(np.exp(1j*np.pi*np.arange(-N/2.,N/2.)) * alpha),np.ones((1,N3))) #EST: My_FRFT_Centered
#        Y = Y * postFactor
 
    else:
        X = X.flatten()  
        N = len(X)
        
        Factor2 = np.exp(1j*np.pi*(np.arange(0,N))) * N * alpha
        x_tilde = X * Factor2
        
        n = np.hstack((np.arange(0,N), np.arange(-N,0,1)))
        Factor = np.exp(-1j * np.pi * alpha * n**2)
        
        x_tilde = np.pad(x_tilde, (0,N),'constant',constant_values=0)
        x_tilde = x_tilde * Factor

        x_tilde = scipy.fftpack.fft(x_tilde,overwrite_x=True)
        YY = scipy.fftpack.fft(np.conjugate(Factor))
        Y = scipy.fftpack.ifft(x_tilde*YY)

        Y = Y * Factor
        Y = Y[:N]
            
    return Y.astype(np.complex64)

def My_FRFT(X,alpha,N,N3):
    n = np.array(range(0,N) + range(-N,0)) #[0:1:N-1, -N:1:-1]'
    n = np.tile(n, (N3,1))
    n = n.transpose()
    Factor = np.exp(-1j * np.pi * alpha * n**2)
    
    x_tilde = np.pad(X,((0, N), (0, 0)),'constant',constant_values=0) 
    x_tilde = x_tilde * Factor    
    
    x_tilde = scipy.fftpack.fft(x_tilde, axis = 0, overwrite_x = True)
    YY = scipy.fftpack.fft(Factor.conjugate(), axis = 0)
    Y = scipy.fftpack.ifft(x_tilde * YY, axis = 0)
    Y = Y * Factor
    Y = Y[:N,:]

    return Y.astype(np.complex64)

def CreateOversampledGrid(kind, N, OS1=1, OS2=1, angleRange = np.pi, plotting = False, figSize = (10,10), color = 'b'):
    """
    This function creates the 'D', 'S' and the 'X' grids such that they are
    oversampled. This program is similar to "Creat_Grid" but uses a different
    parametrization in line with the idea of oversampling. 
    'D' corresponds to Recto-Polar grid, 'S'  for variation on 'D'
    Recto-Polar that has uniform angle sampling, and 'X' for the Polar grid that 
    is obtained from the 'S' Recto-Polar by a uniform steps along all rays.
    
    Synopsis: GridX, GridY = Create_Grid(kind, N, angleRange = np.pi, OS1, OS2, plot = False)
    
    Ported from MATLAB package Polar Lab
    
    Parameters
    ----------
        kind: 
            'D' for Donoho and Averbuch Recto-Polar grid, 
            'S'  for variation on 'D' Recto-Polar that has uniform angle sampling
            'X' for the Polar grid that is obtained from the 'S' Recto-Polar by a uniform steps along all rays.
        N: 
            inital matrix size, the one we will perform Polar FFT 
        angleRange:
            pi or 2pi range of our polar angles 
        OS1:
            oversampling factor along rows
        OS2:
            oversampling factor along columns
        plot:
            boolean flag to show a graph of the grid points or not. Default value false
        figSize:
            size of the plotted figure
        color:
            color and marker style of the plotted figure        
    
    Returns
    ----------
        GridX:
            two 2D array containing the X grid points
        GridY:
            two 2D array containing the Y grid points    
    Example
    ---------- 
           Xp1,Yp1 = CreateOversampledGrid('D',8,2,2,np.pi,True,(10,10),r)  
           Xp2,Yp2 = CreateOversampledGrid('D',16,1,1,np.pi,True) 
    
           Xp1,Yp1 = CreateOversampledGrid('S',8,2,2,np.pi,True) 
           Xp2,Yp2 = CreateOversampledGrid('S',16,1,1,np.pi,True) 
     
           Xp1,Yp1 = CreateOversampledGrid('X',8,2,5,np.pi,True)  
           Xp2,Yp2 = CreateOversampledGrid('X',8,2,2,np.pi,True) 
    
     Written by Miki Elad on Mrch 20th 2005, based on programs written by Dave Donoho.
    """
    kinds = ['D', 'S', 'X']
    if kind not in kinds:
        raise ValueError("Invalid polar FFT Grid. Expected one of: %s" % kinds)
        
    ranges = [np.pi, 2*np.pi]
    if angleRange not in ranges:
        raise ValueError("Invalid polar FFT Grid range. Expected one of: %s" % ranges)     
               
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
    styles = ['-', '--', '-.', ':', '.', ',', 'o', 'v', '^', '<', '>', '1', '2',
        '3', '4', 's', 'p', '*', 'h', 'H', '+', 'x', 'D', 'd', '|', '_']
    SetStyles = set(styles)
    SetColors = set(colors)
    if not len(SetColors.intersection(set(list(color)))) and len(SetStyles.intersection(set(list(color)))):
        raise ValueError("Invalid plotting style")     

    MaxRadius = angleRange

    if kind == 'D':  
        print 'Creating Recto-Polar by Donoho and Averbuch'
        sys.stdout.flush()
            
#        VX = np.zeros((2*N*OS1,N*OS2), dtype =np.float32)
#        VY = np.zeros((2*N*OS1,N*OS2), dtype = np.float32)
        ll = np.arange(-N*OS1, N*OS1, dtype = np.float32)
        theta_y = np.pi * ll /(N*OS1)
        m = np.arange(-N*OS2/2, N*OS2/2, dtype = np.float32)

#        for k in range(ll.shape[0]):
#            theta_x = 2*m*theta_y[k]/(N*OS2)    
#            VX[k,:] = theta_x
#            VY[k,:] = theta_y[k]
        VY = np.vstack(theta_y)
        VY = np.tile(VY,(1,N*OS2))
        VX = np.tile(m,(2*OS1*N, 1))
        VX = 2 * VX * VY/(N*OS2) 
       
#        HX = np.zeros((2*N*OS1,N*OS2), dtype =np.float32)
#        HY = np.zeros((2*N*OS1,N*OS2), dtype =np.float32)
        ll = np.arange(-N*OS1, N*OS1, dtype = np.float32)
        theta_x = np.pi * ll /(N*OS1)
        m = np.arange(-N*OS2/2 + 1, N*OS2/2 + 1, dtype = np.float32)
       
#        for k in range(theta_x.shape[0]):
#            theta_y = 2*m*theta_x[k]/(N*OS2)    
#            HX[k,:] = theta_x[k]
#            HY[k,:] = theta_y
        HX = np.vstack(theta_x)
        HX = np.tile(HX,(1,N*OS2))          
        HY = np.tile(m,(2*OS1*N, 1))
        HY = 2 * HY * HX/(N*OS2) 

        GridX = np.hstack((np.fliplr(VX),HX)) # we have to flip in order to get a smoothly rotating rays
        GridY = np.hstack((np.fliplr(VY),HY)) 
        
        GridX = GridX * MaxRadius/np.pi
        GridY = GridY * MaxRadius/np.pi
        
        if plotting:
            fig, ax = plt.subplots(figsize = figSize)
            ax.plot(GridX,GridY,color)
            ax.axes.set_xlim(-MaxRadius, MaxRadius)
            ax.axes.set_ylim(-MaxRadius, MaxRadius)
            ax.set_aspect('equal', 'box')
            fig.tight_layout()
            fig.show()

    if kind == 'S':  
        print 'Changing the recto-polar to have uniform angle sampling'
        sys.stdout.flush()
            
#        VX = np.zeros((2*N*OS1,N*OS2), dtype =np.float32)
#        VY = np.zeros((2*N*OS1,N*OS2), dtype = np.float32)
        ll = np.arange(-N*OS1, N*OS1, dtype = np.float32)
        theta_y = np.pi * ll /(N*OS1)
        m = np.arange(-N*OS2/2, N*OS2/2, dtype = np.float32)

#        for k in range(ll.shape[0]):
#            theta_x = theta_y[k]*np.tan(m*np.pi/(N*OS2)/2) 
#            VX[k,:] = theta_x
#            VY[k,:] = theta_y[k]
        VX = np.vstack(theta_y)
        VX = np.tile(VX,(1,N*OS2))
        VX = VX * np.tile(np.tan(m*np.pi/(N*OS2)/2), (2*OS1*N, 1))         
        VY = np.vstack(theta_y)
        VY = np.tile(VY, (1,N*OS2))
                 
#        HX = np.zeros((2*N*OS1,N*OS2), dtype =np.float32)
#        HY = np.zeros((2*N*OS1,N*OS2), dtype =np.float32)
        ll = np.arange(-N*OS1, N*OS1, dtype = np.float32)
        theta_x = np.pi * ll /(N*OS1)
        m = np.arange(-N*OS2/2 + 1, N*OS2/2 + 1, dtype = np.float32)
       
#        for k in range(theta_x.shape[0]):
#            theta_y = theta_x[k]*np.tan(m*np.pi/(N*OS2)/2)   
#            HX[k,:] = theta_x[k]
#            HY[k,:] = theta_y
        HX = np.vstack(theta_x)
        HX = np.tile(HX, (1,N*OS2)) 
        HY = np.vstack(theta_x)
        HY = np.tile(HY,(1,N*OS2))
        HY = HY * np.tile(np.tan(m*np.pi/(N*OS2)/2), (2*OS1*N, 1))         
    

        GridX = np.hstack((np.fliplr(VX),HX)) # we have to flip in order to get a smoothly rotating rays
        GridY = np.hstack((np.fliplr(VY),HY)) 
        
        GridX = GridX * MaxRadius/np.pi
        GridY = GridY * MaxRadius/np.pi
        
        if plotting:
            fig, ax = plt.subplots(figsize = figSize)
            ax.plot(GridX,GridY,color)
            ax.axes.set_xlim(-MaxRadius, MaxRadius)
            ax.axes.set_ylim(-MaxRadius, MaxRadius)
            ax.set_aspect('equal', 'box')
            fig.tight_layout()
            fig.show()
 
    if kind == 'X':
        print 'Creating Polar grid, generated by warping the recto-polar along angles and then rays'
        sys.stdout.flush()
            
#        VX = np.zeros((2*N*OS1,N*OS2), dtype =np.float32)
#        VY = np.zeros((2*N*OS1,N*OS2), dtype = np.float32)
        ll = np.arange(-N*OS1, N*OS1, dtype = np.float32)
        theta_y = np.pi * ll /(N*OS1)
        m = np.arange(-N*OS2/2, N*OS2/2, dtype = np.float32)

#        for k in range(ll.shape[0]):
#            theta_x = theta_y[k]*np.tan(m*np.pi/(N*OS2)/2) 
#            VX[k,:] = theta_x
#            VY[k,:] = theta_y[k]
        VX = np.vstack(theta_y)
        VX = np.tile(VX,(1,N*OS2))
        VX = VX * np.tile(np.tan(m*np.pi/(N*OS2)/2), (2*OS1*N, 1))         
        VY = np.vstack(theta_y)
        VY = np.tile(VY, (1,N*OS2))
                 
#        HX = np.zeros((2*N*OS1,N*OS2), dtype =np.float32)
#        HY = np.zeros((2*N*OS1,N*OS2), dtype =np.float32)
        ll = np.arange(-N*OS1, N*OS1, dtype = np.float32)
        theta_x = np.pi * ll /(N*OS1)
        m = np.arange(-N*OS2/2 + 1, N*OS2/2 + 1, dtype = np.float32)
       
#        for k in range(theta_x.shape[0]):
#            theta_y = theta_x[k]*np.tan(m*np.pi/(N*OS2)/2)   
#            HX[k,:] = theta_x[k]
#            HY[k,:] = theta_y
        HX = np.vstack(theta_x)
        HX = np.tile(HX, (1,N*OS2)) 
        HY = np.vstack(theta_x)
        HY = np.tile(HY,(1,N*OS2))
        HY = HY * np.tile(np.tan(m*np.pi/(N*OS2)/2), (2*OS1*N, 1))         
    

        GridX = np.hstack((np.fliplr(VX),HX)) # we have to flip in order to get a smoothly rotating rays
        GridY = np.hstack((np.fliplr(VY),HY)) 
        
        for k in range(2*N*OS1):
            Factor = np.sqrt(GridX[k,:]**2 + GridY[k,:]**2)
            if min(Factor>0): 
                Factor = Factor/min(Factor)
                GridX[k,:] = GridX[k,:]/Factor
                GridY[k,:] = GridY[k,:]/Factor   
        
        if plotting:
            fig, ax = plt.subplots(figsize = figSize)
            ax.plot(GridX,GridY,color)
            ax.axes.set_xlim(-MaxRadius, MaxRadius)
            ax.axes.set_ylim(-MaxRadius, MaxRadius)
            ax.set_aspect('equal', 'box')
            fig.tight_layout()
            fig.show() 
    
    return GridX, GridY 
