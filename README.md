# README #


### What is this repository for? ###

These codes represent a tool for estimating the fractal dimension of a given 3D image with the box counting and correlation dimension methods. 
The codes have been used for the AJP Submitted paper. 

### How do I get set up? ###

The repository will soon be updated with examples on how to run the code. 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Ioannis Vogiatzis Oikonomidis is the author and owner of the codes. Any correspondance concerning the scripts can be sent to ioannis.vogiatzis.oikonomidis@outlook.com